import csv


class Forca(object):

    def __init__(self):
        msg = '/home/fabiano/PycharmProjects/jogo-forca/base/msg.txt'
        phases = '/home/fabiano/PycharmProjects/jogo-forca/base/pergunta.csv'
        self.msg = msg
        self.phrases = phases

    def welcome(self):
        """
        Imprime mensagem de boas vindas
        :return: msg welcome
        """
        f_msg = open(self.msg)
        welcome = ''
        for i in range(0, 4):
            welcome += f_msg.readline()
        return welcome

    def input_field(self, msg, type_):
        """ Function que recebe uma msg para o input e converte para um formato especifico

        :param msg: msg input
        :param type_:  type input
        :return: variável com input
        """

        try:
            field = type_(input(msg))
        except Exception:
            field = self.input_field(msg, type_)
        return field

    def manager(self):
        """ Manager game forca

        :return:
        """
        msg = 'Digite 0 ou 1'
        type_ = int
        choice = self.input_field(msg, type_)
        if choice == 0:
            phase = input('Digite uma palavra: ')
            clew = input('De uma dica: ')
            self.insert_phase(phase=phase, clew=clew)
        else:
            return False

    def insert_phase(self, phase, clew):
        """ Recivied phase and clew and insert file

        :param phase: palavra a ser descoberta <str>
        :param clew: dica para resolver a forca <str>
        :return:
        """
        file_ = open(self.phrases, 'a')
        file_.write(phase + "\n")
        file_.write(clew + "\n")
        file_.close()

    def play_game(self):
        lista = []
        with open(self.phrases, 'r') as f:
           for i in f:
               lista.append(i)


if __name__ == "__main__":
    forca = Forca()
    wel = forca.welcome()
    print(wel)
    start = forca.manager()
    print(start)

