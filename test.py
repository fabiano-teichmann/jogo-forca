from unittest import TestCase, main
from main import Forca

class Teste(TestCase):


    def test_input(self):
        forca = Forca()
        # esta dando erro que terei que verificar mas já chequei e tratei para não ter input invalido no jogo
        self.assertRaises(ValueError, forca.input_field(msg='Digite int', type_=int), 'Experado numero inteiro')


if __name__ == '__main__':
    main()